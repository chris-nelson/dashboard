//
//  DashboardRootViewController.m
//  DASHboard
//
//  Created by Christopher Nelson on 11/23/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import "DashboardRootViewController.h"

@interface DashboardRootViewController ()

@end

@implementation DashboardRootViewController

+ (DashboardRootViewController*) rootViewController
{
    DashboardRootViewController* rootViewController = (DashboardRootViewController*)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    return rootViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
