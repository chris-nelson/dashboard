//
//  DashboardDataObject.m
//  DASHboard
//
//  Created by Christopher Nelson on 11/23/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import "DashboardDataObject.h"

@implementation DashboardDataObject


+ (instancetype)sharedData
{
    static DashboardDataObject* sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{

        sharedInstance = [[DashboardDataObject alloc] init];
    
    });
    
    return sharedInstance;
}



@end
