//
//  DashboardSettingsObject.m
//  DASHboard
//
//  Created by Christopher Nelson on 11/23/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import "DashboardSettingsObject.h"

@implementation DashboardSettingsObject

+ (instancetype)sharedSettings
{
    static DashboardSettingsObject* sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        
        sharedInstance = [[DashboardSettingsObject alloc] init];
        
    });
    
    return sharedInstance;
}

@end
