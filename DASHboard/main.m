//
//  main.m
//  DASHboard
//
//  Created by Christopher Nelson on 11/23/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
