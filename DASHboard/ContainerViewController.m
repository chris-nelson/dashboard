//
//  ContainerViewController.m
//  DASHboard
//
//  Created by Christopher Nelson on 11/23/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import "ContainerViewController.h"

@interface ContainerViewController ()

@property (nonatomic, weak) UIViewController *currentViewController;

@end

@implementation ContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) displayViewControllerInContainer:(UIViewController*)viewcontroller completion:(void (^)(BOOL finished))completion
{
    [self swapFromViewController:self.currentViewController toViewController:viewcontroller completion:completion];
}

- (void)swapFromViewController:(UIViewController*)fromViewController toViewController:(UIViewController*)toViewController completion:(void (^)(BOOL finished))completion
{
    [fromViewController willMoveToParentViewController:nil];
    
    toViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    toViewController.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:toViewController];
    [fromViewController.view removeFromSuperview];
    [self.view addSubview:toViewController.view];
    
    [UIView transitionWithView:self.view
                      duration:0.2
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        self.currentViewController = toViewController;
                    }
                    completion:^(BOOL finished) {
                        
                        if(completion)
                        {
                            completion(finished);
                        }
                    }];
}

@end
