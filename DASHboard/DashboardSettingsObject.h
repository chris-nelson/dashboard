//
//  DashboardSettingsObject.h
//  DASHboard
//
//  Created by Christopher Nelson on 11/23/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DashboardSettingsObject : NSObject

+ (instancetype)sharedSettings;

@end
